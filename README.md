pyMapperGUI
========

Cross-platform GUI for libmapper

[![pymappergui](http://acaia.ca/~tiago/images/pymapper.png "pymappergui")](https://github.com/tiagovaz/pymappergui)
